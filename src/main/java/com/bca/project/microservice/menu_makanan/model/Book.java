/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author bituser
 */
@Entity
@Table(name = "book")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Book {
    @Id
    @Column(name = "id_book")
    private int id;
    
    @Column(name = "status")
    private int status;
    
    @Column(name = "tanggal")
    private Date tanggal;
    
    @Column(name = "id_user")
    private int idUser;
    
    @Column(name = "id_menu")
    private int idMenu;
    
    public Book(){
	
    }
    public Book(int idUser, int idMenu,int status){
	this.status=status;
	this.idUser=idUser;
	this.idMenu=idMenu;
    }
}
