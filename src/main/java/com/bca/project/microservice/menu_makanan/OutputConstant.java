/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan;

/**
 *
 * @author bituser
 */
public class OutputConstant {
    public static final String OK = "ok";
    public static final String NOT_FOUND = "not found";
    public static final String INTERNAL_ERROR = "internal error";
    public static final String FORBIDDEN = "forbidden";
    public static final String BOOK_NOT_FOUND = "belum ada booking";
    public static final String URL = "http://10.1.128.241:8090/user/is-exist";
}
