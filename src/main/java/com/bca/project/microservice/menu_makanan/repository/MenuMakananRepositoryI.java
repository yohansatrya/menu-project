/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.repository;

import com.bca.project.microservice.menu_makanan.model.Book;
import com.bca.project.microservice.menu_makanan.model.Menu;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bituser
 */
@Repository
public class MenuMakananRepositoryI implements MenuMakananRepository{
    @Autowired private MenuRepository mr;
    @Autowired private BookRepository br;

    @Override
    public List<Menu> getMenu(String hari) {
	List<Menu> m = new ArrayList<Menu>();
	m = mr.findByHari(hari.toUpperCase());
	return m;
    }
    
    @Override
    public Menu getMenu(int id) {
	Menu m = new Menu();
	m = mr.findById(id);
	return m;
    }

    @Override
    public Book getBook(int id,Date tgl) {
	List<Book> b = new ArrayList<Book>();
	Book hasil=null;
	b = br.findByIdUser(id);
	for(Book  i : b){
	    try{
		String tanggal = i.getTanggal().toString().substring(0, 10);
		if(tanggal.equals(tgl.toString())&&i.getStatus()==1){
		    hasil=i;
		}
	    }
	    catch(Exception e){
		return null;
	    }
	}
	return hasil;
    }
    
    @Override
    public List<Book> getBook(int id) {
	List<Book> b = new ArrayList<Book>();
	b = br.findByIdUser(id);
	return b;
    }

    public void insertBook(Book b){
	br.save(b);
    }
    
    public int cekJumlah(int idMenu){
	int jml = mr.findById(idMenu).getJumlah();
	return jml;
    }
    
    public void updateMenu(Menu jml){
	mr.save(jml);
    }
    
    public void updateBook(Book stat){
	br.save(stat);
    }
}
