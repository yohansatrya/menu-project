/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.service;

import com.bca.project.microservice.menu_makanan.model.Book;
import com.bca.project.microservice.menu_makanan.model.Output;
import org.springframework.stereotype.Service;

/**
 *
 * @author bituser
 */
@Service
public interface BookService {
    public Output insertBook(int idUser,int idMenu,String hari);
    public Output findBook(int idUser);
    public Output findBookC(int idUser);
    public Output cancelBook(int idUser);
}
