/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.repository;

import com.bca.project.microservice.menu_makanan.model.Book;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bituser
 */
@Repository
public interface BookRepository extends CrudRepository<Book, Integer>{
    List<Book> findByIdUser(int id);
}
