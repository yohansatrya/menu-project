/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.service;

import com.bca.project.microservice.menu_makanan.OutputConstant;
import com.bca.project.microservice.menu_makanan.model.Book;
import com.bca.project.microservice.menu_makanan.model.Menu;
import com.bca.project.microservice.menu_makanan.model.Output;
import com.bca.project.microservice.menu_makanan.repository.MenuMakananRepository;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bituser
 */
@Service
public class MenuServiceI implements MenuService {
    
    OutputConstant c;

    Output o = new Output();
    
    @Autowired BookService bs;

    @Autowired
    private MenuMakananRepository mmr;

    @Override
    public Output getMenuHari() {
	o.setData(mmr.getMenu(getHari()));
	o.setResponse_code(200);
	o.setKeterangan(c.OK);
	return o;
    }

    @Override
    public Output getMenuHari(String hari) {
	if (!hari.equals("")) {
	    o.setData(mmr.getMenu(hari));
	    o.setResponse_code(200);
	    o.setKeterangan(c.OK);
	    return o;
	}
	o.setData(mmr.getMenu(getHari()));
	o.setResponse_code(200);
	o.setKeterangan(c.OK);
	return o;
    }

    public String getHari() {
	String[] nama_hari = {"Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"};
	Calendar cal = Calendar.getInstance();
	DateFormat dateFormat = new SimpleDateFormat("u");
	int a = Integer.parseInt(dateFormat.format(cal.getTime()));
	String hari = nama_hari[a - 1];
	return hari;
    }
    
    public Output insertBooking(int idUser, int idMenu) {
	o=bs.insertBook(idUser,idMenu,getHari());
	return o;
    }
    
    public Output findBook(int idUser){
	o=bs.findBookC(idUser);
	return o;
    }
    
    public Output cancelBooking(int idUser){
	o=bs.cancelBook(idUser);
	return o;
    }
}
