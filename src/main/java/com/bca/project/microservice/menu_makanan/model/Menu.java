/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author bituser
 */
@Entity
@Table(name = "menu")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Menu {
    @Id
    @Column(name = "id_menu")
    private int id;
    
    @Column(name = "nama_makanan")
    private String makanan;
    
    @Column(name = "jumlah")
    private int jumlah;
    
    @Column(name = "hari")
    private String hari;
    
    public Menu(){
	
    }
    public Menu(int jml){
	this.jumlah=jml;
    }
    public void setJumlah(int jml){
	this.jumlah=jml;
    }
    public int getJumlah(){
	return jumlah;
    }
}
