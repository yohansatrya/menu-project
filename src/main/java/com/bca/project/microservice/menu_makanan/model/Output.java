/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.model;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author bituser
 */
@Getter
@Setter
public class Output {
    private Object data;
    private int response_code;
    private String keterangan;
    public Output(){
	
    }
    public void setData(Object data){
	this.data=data;
    }
    public Object getData(){
	return data;
    }
}
