/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.controller;

import com.bca.project.microservice.menu_makanan.model.Input;
import com.bca.project.microservice.menu_makanan.model.Output;
import com.bca.project.microservice.menu_makanan.service.BookService;
import com.bca.project.microservice.menu_makanan.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bituser
 */
@RestController
@RequestMapping("menu")
public class MenuController {
    @Autowired private MenuService ms;
    
    Output o = new Output();
    
//    @GetMapping("")
//    public Output menu(){
//	o = ms.getMenuHari();
//	return o;
//    }
//    
    @GetMapping("hari")
    public Output menu1(@RequestParam String hari){
	o = ms.getMenuHari(hari);
	return o;
    }
    
    @PostMapping("book")
    public Output book(@RequestBody Input input){
	o = ms.insertBooking(input.getIdUser(), input.getIdMenu());
	return o;
    }
    
    @PostMapping("find")
    public Output find(@RequestBody Input input){
	o = ms.findBook(input.getIdUser());
	return o;
    }
    
    @PostMapping("cancel")
    public Output cancel(@RequestBody Input input){
	o = ms.cancelBooking(input.getIdUser());
	return o;
    }
}
