/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.service;

import com.bca.project.microservice.menu_makanan.model.Output;

/**
 *
 * @author bituser
 */
public interface MenuService {
    public Output getMenuHari();
    public Output getMenuHari(String hari);
    public Output insertBooking(int idUser,int idMenu);
    public Output findBook(int idUser);
    public Output cancelBooking(int idUser);
}
