/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.repository;

import com.bca.project.microservice.menu_makanan.OutputConstant;
import com.bca.project.microservice.menu_makanan.model.InputU;
import com.bca.project.microservice.menu_makanan.model.OutputU;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author bituser
 */
@Repository
public class UserRepository {
    OutputConstant c;
    public boolean cekUser(int id){
	RestTemplate rt = new RestTemplate();
	
	InputU input = new InputU();
	input.setId(id);

	ResponseEntity<OutputU> re = null;
	
	try{
	    re = rt.postForEntity(c.URL, input, OutputU.class);
	    return re.getBody().isData();
	}catch(Exception e){
	    return false;
	}
    }
}
