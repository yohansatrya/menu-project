/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.repository;

import com.bca.project.microservice.menu_makanan.model.Menu;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bituser
 */
@Repository
public interface MenuRepository extends CrudRepository<Menu, Integer>{
    List<Menu> findByHari(String hari);
    Menu findById(int id);
}
