/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.repository;

import com.bca.project.microservice.menu_makanan.model.Book;
import com.bca.project.microservice.menu_makanan.model.Menu;
import java.util.Date;
import java.util.List;

/**
 *
 * @author bituser
 */
public interface MenuMakananRepository{
    public List<Menu> getMenu(String hari);
    public Book getBook(int id,Date tgl);
    public List<Book> getBook(int id);
    public Menu getMenu(int id);
    public void insertBook(Book b);
    public int cekJumlah(int id);
    public void updateMenu(Menu m);
    public void updateBook(Book b);
}
