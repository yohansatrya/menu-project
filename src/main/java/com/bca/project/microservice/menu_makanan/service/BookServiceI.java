/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.project.microservice.menu_makanan.service;

import com.bca.project.microservice.menu_makanan.OutputConstant;
import com.bca.project.microservice.menu_makanan.model.Book;
import com.bca.project.microservice.menu_makanan.model.Menu;
import com.bca.project.microservice.menu_makanan.model.Output;
import com.bca.project.microservice.menu_makanan.repository.MenuMakananRepository;
import com.bca.project.microservice.menu_makanan.repository.UserRepository;
import java.text.ParseException;
import java.sql.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bituser
 */
@Service
public class BookServiceI implements BookService {

    OutputConstant c;

    Output o = new Output();

    @Autowired
    private MenuMakananRepository mmr;

    @Autowired
    private UserRepository ur;

    @Override
    public Output insertBook(int idUser, int idMenu, String hari) {
	List<Menu> lm = mmr.getMenu(hari);
	Menu m = null;
	for (Menu i : lm) {
	    if (i.getId() == idMenu) {
		m = mmr.getMenu(idMenu);
		break;
	    }
	}
	if (m == null) {
	    m = mmr.getMenu(idMenu);
	    if (m == null) {
		o.setData("menu tidak ditemukan");
		o.setResponse_code(404);
		o.setKeterangan(c.NOT_FOUND);
		return o;
	    }
	    o.setData("menu dengan id " + idMenu + " tidak tersedia hari ini");
	    o.setResponse_code(404);
	    o.setKeterangan(c.NOT_FOUND);
	    return o;
	}
	if (!ur.cekUser(idUser)) {
	    o.setData("user tidak ditemukan");
	    o.setResponse_code(404);
	    o.setKeterangan(c.NOT_FOUND);
	    return o;
	}
	o = findBook(idUser);
	if (o.getResponse_code() == 200) {
	    if (m.getJumlah() < 1) {
		o.setData("menu habis");
		o.setResponse_code(404);
		o.setKeterangan(c.NOT_FOUND);
		return o;
	    }
	    mmr.insertBook((new Book(idUser, idMenu, 1)));
	    m.setJumlah(mmr.cekJumlah(idMenu) - 1);
	    mmr.updateMenu(m);
	    o.setData("berhasil booking");
	    return o;
	}
	return o;
    }

    @Override
    public Output findBook(int idUser) {
	try {
	    Book b = mmr.getBook(idUser, hariIni());
	    if (b != null) {
		o.setData("user sudah booking hari ini");
		o.setResponse_code(403);
		o.setKeterangan(c.FORBIDDEN);
		return o;
	    }
	    o.setData(b);
	    o.setResponse_code(200);
	    o.setKeterangan(c.OK);
	    return o;
	} catch (ParseException ex) {
	    o.setData(ex);
	    o.setResponse_code(500);
	    o.setKeterangan(c.INTERNAL_ERROR);
	    return o;
	}
    }

    public Output findBookC(int idUser) {
	try {
	    if (!ur.cekUser(idUser)) {
		o.setData("user tidak ditemukan");
		o.setResponse_code(404);
		o.setKeterangan(c.NOT_FOUND);
		return o;
	    }
	    Book b = mmr.getBook(idUser, hariIni());
	    if (b == null) {
		o.setData(null);
		o.setResponse_code(404);
		o.setKeterangan(c.BOOK_NOT_FOUND);
		return o;
	    }
	    o.setData(b);
	    o.setResponse_code(200);
	    o.setKeterangan(c.OK);
	    return o;
	} catch (ParseException ex) {
	    o.setData(ex);
	    o.setResponse_code(500);
	    o.setKeterangan(c.INTERNAL_ERROR);
	    return o;
	}
    }

    public Output cancelBook(int idUser) {
	if (!ur.cekUser(idUser)) {
	    o.setData("user tidak ditemukan");
	    o.setResponse_code(404);
	    o.setKeterangan(c.NOT_FOUND);
	    return o;
	}
	o = findBookC(idUser);
	Book b = (Book) o.getData();
	if (b == null) {
	    return o;
	}
	b.setStatus(0);
	Menu m = mmr.getMenu(b.getIdMenu());
	m.setJumlah(m.getJumlah()+1);
	mmr.updateBook(b);
	mmr.updateMenu(m);
	o.setData("Booking dibatalkan");
	return o;
    }

    public static Date hariIni() throws ParseException {
	try {
	    long millis = System.currentTimeMillis();
	    java.sql.Date date = new java.sql.Date(millis);
	    return date;
	} catch (Exception e) {
	    return null;
	}
    }
}
