CREATE TABLE menu (
  id_menu int NOT NULL,
  nama_makanan varchar(10) NOT NULL,
  jumlah int NOT NULL DEFAULT '0',
  hari varchar(10) NOT NULL
);

ALTER TABLE menu
  ADD PRIMARY KEY (id_menu);

CREATE TABLE book (
  id_book int NOT NULL,
  status int NOT NULL DEFAULT '1',
  tanggal timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  id_user int NOT NULL,
  id_menu int NOT NULL
);

ALTER TABLE book
  ADD PRIMARY KEY (id_book);

ALTER TABLE book
    ADD CONSTRAINT id_menu_fk 
    FOREIGN KEY (id_menu) 
    REFERENCES menu(id_menu) 
    ON DELETE RESTRICT ON UPDATE RESTRICT;